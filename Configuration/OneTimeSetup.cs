﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpAssignment_Oct17.Configuration
{
    class OneTimeSetup
    {
        public static string appURL = "http://automationpractice.com/index.php";
        public static string Browser = "chrome";
        public static string csvFilePath = @"D:\Essentials\Assignment\Assignment2\Assignment2\TestData\Data.csv";

        //public static string reportPath = @"D:\Essentials\ExtentReports";
    }
}
