﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpAssignment_Oct17.Helpers
{
    class CommonFunctions
    {
        //wait for element to be displayed
        public void WaitForElementToBePresent(IWebElement webElement, string text, int timeSpan)
        {
            WebDriverWait wait = new WebDriverWait(FrameWork.driver, TimeSpan.FromMilliseconds(timeSpan));
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.TextToBePresentInElement(webElement, text));
        }
        //
        public void HardWait(int timeSpan)
        {
            WebDriverWait wait = new WebDriverWait(FrameWork.driver, TimeSpan.FromMilliseconds(timeSpan));

        }
    }
}
