﻿using CSharpAssignment_Oct17.Pages;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpAssignment_Oct17.Tests
{
    public class Assignement : TestBase
    {

        [Test]
        public void AssignmentOneExecution()
        {
            HomePage home = new HomePage();
            home.HoverAndClickingBuildAutomationLabel();
            BuildingAutomationPage build = new BuildingAutomationPage();
            build.ClickOnBuildAutomationENewsletter();
            build.GetNewsTitles();
        }

        [Test]
        public void AssignmentTwoExecution()
        {
            HomePage home = new HomePage();
            home.ClickOnProductsLable();
            ProductsPage product = new ProductsPage();
            product.GetTitle(product.ProductSearchTitle);
            product.VerifyTitle(product.finalText,"Product Search - Automation, Control & Instrumentation Products");

        }

        [Test]
        public void AssignmentThreeExecution()
        {
            HomePage home = new HomePage();
            home.HoverAndClickingSalarySurveyListPage();
            SalarySurveyListPage salary = new SalarySurveyListPage();
            salary.GetRegion("United States");
            salary.RegionOfUS("Pacific (West)");
            
        }


    }
}
