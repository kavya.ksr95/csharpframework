﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpAssignment_Oct17.Pages
{
    public class BuildingAutomationPage
    {
        #region Elements

        private IWebElement BuildAutomationENewsletter = FrameWork.driver.FindElement(By.XPath("//div[@class='holder']/ul[@class='sub-nav']/li/a[contains(@href,'e-newsletter')]"));
        IReadOnlyCollection<IWebElement> listLetters = FrameWork.driver.FindElements(By.XPath("//div[@class='company-section add']//ul/li"));

        #endregion

        #region Methods
        public void GetNewsTitles()
        {
            Console.WriteLine("Total news = " + listLetters.Count);

            foreach (var letters in listLetters)
            {
                Console.WriteLine(letters.Text);
            }
        }
        public void ClickOnBuildAutomationENewsletter()
        {
            BuildAutomationENewsletter.Click();
        }
        #endregion
    }
}
