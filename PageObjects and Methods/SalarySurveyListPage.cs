﻿using OpenQA.Selenium;
using System;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpAssignment_Oct17.Pages
{
    class SalarySurveyListPage
    {
        #region Method
        //Method to get the avg salary of respective regions 
        public void GetRegion(string region)
        {
            switch (region)
            {
                case "United States":
                    IWebElement AvgSal1 = FrameWork.driver.FindElement(By.CssSelector("table:nth-child(11) tbody:nth-child(1) tr:nth-child(2) > td:nth-child(2)"));
                    Console.WriteLine("The Average salary of United State is" + AvgSal1.Text);
                    break;
                case "Canada":
                    IWebElement AvgSal2 = FrameWork.driver.FindElement(By.CssSelector("table:nth-child(11) tbody:nth-child(1) tr:nth-child(3) > td:nth-child(2)"));
                    Console.WriteLine("The Average salary of Canada is" + AvgSal2.Text);
                    break;
                case "Mexico":
                    IWebElement AvgSal3 = FrameWork.driver.FindElement(By.CssSelector("table:nth-child(11) tbody:nth-child(1) tr:nth-child(4) > td:nth-child(2)"));
                    Console.WriteLine("The Average salary of Mexico is" + AvgSal3.Text);
                    break;
                case "Central America (including Caribbean)":
                    IWebElement AvgSal4 = FrameWork.driver.FindElement(By.CssSelector("table:nth-child(11) tbody:nth-child(1) tr:nth-child(5) > td:nth-child(2)"));
                    Console.WriteLine("The Average salary of United State is" + AvgSal4.Text);
                    break;
                case "South America":
                    IWebElement AvgSal5 = FrameWork.driver.FindElement(By.CssSelector("table:nth-child(11) tbody:nth-child(1) tr:nth-child(6) > td:nth-child(2)"));
                    Console.WriteLine("The Average salary of South America is" + AvgSal5.Text);
                    break;
                case "Europe (Western)":
                    IWebElement AvgSal6 = FrameWork.driver.FindElement(By.CssSelector("table:nth-child(11) tbody:nth-child(1) tr:nth-child(7) > td:nth-child(2)"));
                    Console.WriteLine("The Average salary of Europe (Western) is" + AvgSal6.Text);
                    break;
                case "Europe (Eastern)":
                    IWebElement AvgSal7 = FrameWork.driver.FindElement(By.CssSelector("table:nth-child(11) tbody:nth-child(1) tr:nth-child(8) > td:nth-child(2)"));
                    Console.WriteLine("The Average salary of Europe (Eastern) is" + AvgSal7.Text);
                    break;
                case "Africa":
                    IWebElement AvgSal8 = FrameWork.driver.FindElement(By.CssSelector("table:nth-child(11) tbody:nth-child(1) tr:nth-child(9) > td:nth-child(2)"));
                    Console.WriteLine("The Average salary of Africa is" + AvgSal8.Text);
                    break;
                case "Middle East":
                    IWebElement AvgSal9 = FrameWork.driver.FindElement(By.CssSelector("table:nth-child(11) tbody:nth-child(1) tr:nth-child(10) > td:nth-child(2)"));
                    Console.WriteLine("The Average salary of Middle East is" + AvgSal9.Text);
                    break;
                case "Australia and New Zealand":
                    IWebElement AvgSal10 = FrameWork.driver.FindElement(By.CssSelector("table:nth-child(11) tbody:nth-child(1) tr:nth-child(11) > td:nth-child(2)"));
                    Console.WriteLine("The Average salary of Australia and New Zealand is" + AvgSal10.Text);
                    break;
                case "Asia & South Pacific":
                    IWebElement AvgSal11 = FrameWork.driver.FindElement(By.CssSelector("table:nth-child(11) tbody:nth-child(1) tr:nth-child(12) > td:nth-child(2)"));
                    Console.WriteLine("The Average salary of Asia & South Pacific is" + AvgSal11.Text);
                    break;
                case "South Asia":
                    IWebElement AvgSal12 = FrameWork.driver.FindElement(By.CssSelector("table:nth-child(11) tbody:nth-child(1) tr:nth-child(13) > td:nth-child(2)"));
                    Console.WriteLine("The Average salary of South Asia is" + AvgSal12.Text);
                    break;
            }
        }

        //Method to get the percent respondents of region US 
        public void RegionOfUS(string regionUS)
        {
            switch (regionUS)
            {
                case "New England (Northeast)":
                    IWebElement precent1 = FrameWork.driver.FindElement(By.CssSelector("table:nth-child(15) tbody:nth-child(1) tr:nth-child(2) > td:nth-child(3)"));
                    Console.WriteLine("The Precent Respondents of New England (Northeast) is" + precent1.Text);
                    break;
                case "Mid-Atlantic (Northeast)":
                    IWebElement precent2 = FrameWork.driver.FindElement(By.CssSelector("table:nth-child(15) tbody:nth-child(1) tr:nth-child(3) > td:nth-child(3)"));
                    Console.WriteLine("The Precent Respondents of Mid-Atlantic (Northeast) is " + precent2.Text);
                    break;
                case "East North Central (Midwest)":
                    IWebElement precent3 = FrameWork.driver.FindElement(By.CssSelector("table:nth-child(15) tbody:nth-child(1) tr:nth-child(4) > td:nth-child(3)"));
                    Console.WriteLine("The Precent Respondents of East North Central (Midwest) is " + precent3.Text);
                    break;
                case "West North Central (Midwest)":
                    IWebElement precent4 = FrameWork.driver.FindElement(By.CssSelector("table:nth-child(15) tbody:nth-child(1) tr:nth-child(5) > td:nth-child(3)"));
                    Console.WriteLine("The Precent Respondents of West North Central (Midwest) is " + precent4.Text);
                    break;
                case "South Atlantic (South)":
                    IWebElement precent5 = FrameWork.driver.FindElement(By.CssSelector("table:nth-child(15) tbody:nth-child(1) tr:nth-child(6) > td:nth-child(3)"));
                    Console.WriteLine("The Precent Respondents of South Atlantic (South) is " + precent5.Text);
                    break;
                case "East South Central (South)":
                    IWebElement precent6 = FrameWork.driver.FindElement(By.CssSelector("table:nth-child(15) tbody:nth-child(1) tr:nth-child(7) > td:nth-child(3)"));
                    Console.WriteLine("The Precent Respondents of East South Central (South) is " + precent6.Text);
                    break;
                case "West South Central (South)":
                    IWebElement precent7 = FrameWork.driver.FindElement(By.CssSelector("table:nth-child(15) tbody:nth-child(1) tr:nth-child(8) > td:nth-child(3)"));
                    Console.WriteLine("The Precent Respondents of West South Central (South) is " + precent7.Text);
                    break;
                case "Mountain (West)":
                    IWebElement precent8 = FrameWork.driver.FindElement(By.CssSelector("table:nth-child(15) tbody:nth-child(1) tr:nth-child(9) > td:nth-child(3)"));
                    Console.WriteLine("The Precent Respondents of Mountain (West) is " + precent8.Text);
                    break;
                case "Pacific (West)":
                    IWebElement precent9 = FrameWork.driver.FindElement(By.CssSelector("table:nth-child(15) tbody:nth-child(1) tr:nth-child(10) > td:nth-child(3)"));
                    Console.WriteLine("The Precent Respondents of Pacific (West) is " + precent9.Text);
                    break;

            }
        }

        #endregion
    }
}
