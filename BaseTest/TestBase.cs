﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;

namespace CSharpAssignment_Oct17
{
    public class TestBase
    {
        [SetUp]
        public void SetUp()
        {
            FrameWork.driver = new ChromeDriver();
            FrameWork.driver.Navigate().GoToUrl("http://www.automation.com/");
            FrameWork.driver.Manage().Window.Maximize();



        }

   
        [TearDown]
        public void TearDown()
        {
            FrameWork.driver.Quit();
            FrameWork.driver.Close();
        }
    }
    public class FrameWork
    {
        [ThreadStatic]
        public static IWebDriver driver;
    }
}
